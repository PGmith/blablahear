(function() {
	'use strict';

	angular.module('blablaApp', ['ui.router', 'ngCookies', 'ngFileUpload'])
		.constant('site', 'http://blablahear.azurewebsites.net')
		.run(['$rootScope', '$location', '$cookieStore', '$http', '$state', '$window', 'RequestStorage', 'ModalService', 'ToastrService', function($rootScope, $location, $cookieStore, $http, $state, $window, RequestStorage, ModalService, ToastrService) {
			$rootScope.globals = $cookieStore.get('globals') || {};

			if ($rootScope.globals.currentUser) {
				$http.defaults.headers.common.Authorization = 'bearer' + ' ' + $rootScope.globals.currentUser.token;
			}

			$rootScope.$on('event:auth-loginRequired', function() {
				ModalService.open('modal-hybrid');
			});

			$rootScope.$on('event:auth-loginConfirmed', function() {
				if (RequestStorage.bufferSize() > 0) {
					RequestStorage.retryAllWithToken($rootScope.globals.currentUser.token);
				}
				$http.defaults.headers.common.Authorization = 'bearer' + ' ' + $rootScope.globals.currentUser.token;
				ToastrService.success('Добро пожаловать');
			});

			$rootScope.$on('event:auth-loginCanceled', function() {
				if (RequestStorage.bufferSize() > 0) {
					RequestStorage.rejectAll('Incorrect login or password');
				}
			});

			$rootScope.$on('event:auth-forbidden', function() {
				ToastrService.warning('Доступ запрещен');
			});

			$rootScope.$on('event:user-UserCreated', function() {
				ToastrService.success('Регистрация прошла успешно');
			});

		}])
		.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'site', function($stateProvider, $urlRouterProvider, $httpProvider, site) {
			$httpProvider.interceptors.push('AuthInterceptor');
			$stateProvider
				.state('users', {
					url: '/users',
					abstract: true,
					templateUrl: 'app/templates/users/users.html'
				})
				.state('users.groups', {
					url: '/:id/groups',
					templateUrl: 'app/templates/users/user_groups.html'
				})
				.state('groups', {
					url: '/groups?offset&count',
					templateUrl: 'app/templates/groups_list.html'
				})
				.state('posts', {
					url: '/groups/:id?offset&count',
					templateUrl: 'app/templates/posts_list.html'
				})
				.state('tags', {
					url: '/posts?q',
					templateUrl: 'app/templates/tags_list.html'
				});

			$urlRouterProvider.otherwise('/groups');
			$urlRouterProvider.when('/', '/groups');
			$urlRouterProvider.when('', '/groups');
		}]);

})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.controller('AuthCtrl', ['$scope', '$state', '$rootScope', '$location', '$window', 'AuthService', 'UserApiService', function($scope, $state, $rootScope, $location, $window, AuthService, UserApiService) {
			var vm = this;
			vm.data = {};
			vm.data.user = '';
			vm.data.status = false;

			vm.login = function() {
				vm.data.status = false;
				AuthService.login(vm.data.user)
					.then(function(response) {
						vm.data.status = true;
						UserApiService.setCredentials(vm.data.user, response.data.access_token);
					}, function(response) {
						vm.data.status = true;
						$rootScope.$broadcast('event:auth-loginCanceled');
					});
			};

			$rootScope.logout = function () {
				AuthService.logout().then(function() {
					$window.location.reload();
					UserApiService.clearCredentials();
				});
			};

		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.controller('GroupsCtrl', ['$scope', '$timeout', 'GroupService', 'ToastrService', function($scope, $timeout, GroupService, ToastrService) {
			var vm = this;
			vm.data = {};
			vm.position = {};
			vm.position.offset = 0;
			vm.position.count = 10;
			vm.data.groups = [];
			vm.data.groupData = {};
			vm.data.loaded = false;
			vm.data.status = false;


			GroupService.getGroups(vm.position)
				.then(function(response) {
					vm.data.groups = response.data;
					vm.data.loaded = true;
				});

			vm.createGroup = function (file) {
				vm.data.status = false;
				GroupService.createGroup(vm.data.groupData, file)
					.then(function(response) {
						vm.data.groups.push(response.data);
						vm.data.status = true;
						ToastrService.success('Группа ' + response.data.short_name + ' была успешно создана');
					});
			};

			vm.follow = function (index, id, is_follow) {
				GroupService.follow(id, is_follow)
					.then(function (response) {
						if (is_follow)
							vm.data.groups[index].followers_count--;
						else 
							vm.data.groups[index].followers_count++;

						vm.data.groups[index].is_follow = !vm.data.groups[index].is_follow;
					});	
			};

			vm.nextScroll = function() {
				vm.data.loaded = false;
				vm.position.offset += 10;
				$timeout(function() {
					GroupService.getGroups(vm.position)
						.then(function(response) {
							vm.data.groups = vm.data.groups.concat(response.data);
							vm.data.loaded = true;
						});
				}, 300);
			};

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('MainCtrl', ['$scope', '$rootScope', 'TagService', function ($scope, $rootScope, TagService) {
			$rootScope.showPanel = false;
			$rootScope.section = 'groups';

			$rootScope.$on('$stateChangeSuccess', function (event, toState) {
				$rootScope.section = toState.name;
			});

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('PostsCtrl', ['$scope', '$timeout', '$stateParams', 'PostService', 'ToastrService', function ($scope, $timeout, $stateParams, PostService, ToastrService) {
			var vm = this;
			vm.data = {};
			vm.position = {};
			vm.position.offset = 0;
			vm.position.count = 10;
			vm.data.posts = [];
			vm.data.postData = {};
			vm.data.loaded = false;
			vm.data.status = false;
			vm.data.groupId = '';
			vm.data.groupName = '';

			PostService.checkGroup()
				.then(function (response) {
					vm.data.groupId = response.data.id;
					vm.data.groupName = response.data.full_name;
					PostService.getPosts(vm.data.groupId, vm.position)
						.then(function (response) {
							vm.data.posts = response.data;
							vm.data.loaded = true;
						});
				});

			vm.createPost = function () {
				vm.data.status = false;
				PostService.createPost(vm.data.postData, vm.data.groupId)
					.then(function (response) {
						vm.data.posts.push(response.data);
						vm.data.status = true;
						ToastrService.success('Сообщение добавлено');
					});
			};

			vm.like = function (index, id, is_liked) {
				PostService.like(id, is_liked)
					.then(function (response) {
						if (is_liked)
							vm.data.posts[index].likes--;
						else 
							vm.data.posts[index].likes++;

						vm.data.posts[index].is_liked = !vm.data.posts[index].is_liked;
					});	
			};

			vm.nextScroll = function() {
				vm.data.loaded = false;
				vm.position.offset += 10;
				$timeout(function() {
					PostService.getPosts(vm.data.groupId, vm.position)
						.then(function(response) {
							vm.data.posts = vm.data.posts.concat(response.data);
							vm.data.loaded = true;
						});
				}, 300);
			};

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('TagsCtrl', ['$scope', '$timeout', '$stateParams', 'TagService', function ($scope, $timeout, $stateParams, TagService) {
			var vm = this;
			vm.data = {};
			vm.data.dataByTag = [];
			vm.data.popularTags = [];
			vm.data.loaded = false;
			vm.data.tagTitle = $stateParams.q;
			vm.position = {};
			vm.position.offset = 0;
			vm.position.count = 10;

			if ($stateParams.q) {
				TagService.getDataByTag(vm.position, vm.data.tagTitle)
					.then(function (response) {
						vm.data.dataByTag = response.data;
						vm.data.loaded = true;
					});
			} else {
				TagService.getPopularTags()
					.then(function(response) {
						vm.data.popularTags = response.data;
					});
			}
			

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('UserGroupsCtrl', ['$scope', '$timeout', '$stateParams', function ($scope, $timeout, $stateParams) {
			var vm = this;
			vm.data = {};
			

		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.controller('UsersCtrl', ['$scope', '$state', '$rootScope', '$location', 'UserService', 'UserApiService', function($scope, $state, $rootScope, $location, UserService, UserApiService) {
			var vm = this;
			vm.data = {};
			vm.data.user = '';
			vm.data.status = false;


			vm.register = function() {
				vm.data.status = false;
				UserService.create(vm.data.user)
					.then(function(response) {
						vm.data.status = true;
						$rootScope.$broadcast('event: auth-UserCreated');
					});
			};


		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.directive('resetForm', ['$timeout', 'ModalService', function($timeout, ModalService) {
			return {
				restrict: 'A',
				require: '^form',
				scope: {
					status: '=',
					formData: '='
				},
				link: function(scope, $elem, $attrs, formCtrl) {
					scope.$watch(function() {
						return scope.status;
					}, function() {
						if (scope.status) {
							ModalService.close();
							formCtrl.$setPristine();
							scope.formData = {};
						}
					});

				}
			};
		}]);

	angular.module('blablaApp')
		.directive('passwordMatch', ['$timeout', function($timeout) {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function(scope, $elem, $attrs, ctrl) {
					$elem.add('.pw1').on('keyup', function() {
						scope.$apply(function() {
							var v = $elem.val() === $('.pw1').val();
							ctrl.$setValidity('pwmatch', v);
						});
					});
				}
			};
		}]);

	angular.module('blablaApp')
		.directive('endlessScroll', ['$timeout', function($timeout) {
			return {
				restrict: 'A',
				scope: {
					nextScroll: '&'
				},
				link: function(scope, $elem, $attrs) {
					$(window).on('scroll', function() {
						if ($(window).scrollTop() === $(document).height() - $(window).height()) {
							scope.nextScroll();
							// if (scope.group.config.allDataLoaded)
							// 	$(window).off('scroll');
						}
					});
				}
			};
		}]);

	angular.module('blablaApp')
		.directive('ngFocus', [function() {
			var FOCUS_CLASS = "ng-focused";
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function(scope, $elem, attrs, ctrl) {
					ctrl.$focused = false;
					$elem.on('focus', function(event) {
						$elem.addClass(FOCUS_CLASS);
						scope.$apply(function() {
							ctrl.$focused = true;
						});
					});
					$elem.on('blur', function(event) {
						$elem.removeClass(FOCUS_CLASS);
						scope.$apply(function() {
							ctrl.$focused = false;
						});
					});
				}
			};
		}]);

	angular.module('blablaApp')
		.directive("masonry", ['$timeout', function($timeout) {
			return {
				scope: {
					itemSelector: '@'
				},
				link: function(scope, elem, attrs) {

					scope.$watch(function() {
							return elem[0].children.length;
						},
						function(newVal) {
							$timeout(function() {
								elem.masonry('reloadItems');
								elem.masonry();
							}, 200);
						});

					elem.masonry({
						itemSelector: scope.itemSelector,
						transitionDuration: 0
					});
					scope.masonry = elem.data('masonry');
				}
			};
		}]);

	angular.module('blablaApp')
		.directive('tabs', [function() {
			return {
				restrict: 'E',
				replace: true,
				transclude: true,
				scope: {
					paneTitles: '=',
					paneAnchors: '=',
					active: '=',
					mainClass: '='
				},
				controller: ['$scope', function($scope) {
					this.paneTitles = [];
					this.paneAnchors = [];
					this.active = $scope.active;
					this.paneTitles = $scope.paneTitles;
					this.paneAnchors = $scope.paneAnchors;
					$scope.select = function(anchor) {
						$scope.active = anchor;
						this.active = $scope.active;
					};
				}],
				link: function(scope, $elem, attrs, ctrl) {

				},
				template: 
				"<div>" +
					"<ul class='nav nav-tabs {{ class }}'>" +
						"<li ng-repeat='pane in paneTitles track by $index' ng-click='select(paneAnchors[$index])'>" +
							"<a href='#{{ paneAnchors[$index] }}' data-toggle='tab'>{{ pane }}</a>" +
						"</li>" +
					"</ul>" +
					"<div class='tab-content' ng-transclude></div>" +
				"</div>"
			};
		}]);

	angular.module('blablaApp')
		.directive('pane', [function() {
			return {
				restrict: 'E',
				replace: true,
				transclude: true,
				scope: {
					anchor: '@'
				},
				controller: ['$scope', function($scope) {
					this.anchor = $scope.anchor;
				}],
				template: "<div class='tab-pane' id='{{ anchor }}' ng-transclude></div>"
			};
		}]);


})();
(function() {
    'use strict';

    angular.module('blablaApp')
        .filter('dateFomat', ['$filter', function($filter) {
            return function (content) {
            	moment.locale("ru");
            	var date = moment(content).fromNow();
                return date;
            };
        }]);
})();
(function() {
    'use strict';

    angular.module('blablaApp')
        .filter('hashtag', ['$filter', '$sce', function($filter, $sce) {
            return function (content) {
                var newContent = content.replace(/(#)(\w*[a-zA-Z_]+\w*)/gim, "<a ui-sref='tags({q: $2})' href='#/posts?q=$2'>$&</a>");
                return $sce.trustAsHtml(newContent);
            };
        }]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('FlashService', ['$rootScope', function($rootScope) {

			(function() {
				$rootScope.$on('$locationChangeStart', function() {
					clearFlashMessage();
				});

				function clearFlashMessage() {
					var flash = $rootScope.flash;
					if (flash) {
						if (!flash.keepAfterLocationChange) {
							delete $rootScope.flash;
						} else {
							flash.keepAfterLocationChange = false;
						}
					}

				}
			})();

			return {
				success: function(keepAfterLocationChange) {
					$rootScope.flash = {
						message: 'Succes',
						type: 'success',
						keepAfterLocationChange: keepAfterLocationChange
					};
				},
				error: function(keepAfterLocationChange) {
					$rootScope.flash = {
						message: 'Error was occured',
						type: 'error',
						keepAfterLocationChange: keepAfterLocationChange
					};
				}
			};

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('ModalService', [function () {
		
			return {
				close: function () {
					$('.modal').modal('hide');
				},
				open: function(name) {
					$('.' + name).modal('show');
				}
			};
		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('RequestStorage', ['$injector', function ($injector) {
			var buffer = [];

			function retryHttpRequest(config, deferred) {
				function successCallback(response) {
					deferred.resolve(response);
				}

				function errorCallback(response) {
					deferred.reject(response);
				}
				var $http = $http || $injector.get('$http');
				$http(config).then(successCallback, errorCallback);
			}

			return {
				push: function(config, deferred) {
					buffer.push({
						config: config,
						deferred: deferred
					});
				},
				bufferSize: function () {
					return buffer.length;
				},
				rejectAll: function(reason) {
					if (reason) {
						for (var i = 0; i < buffer.length; i++) {
							buffer[i].deferred.reject(reason);
						}
					}
					buffer = [];
				},
				retryAllWithToken: function(token) {
					for (var i = 0; i < buffer.length; i++) {
						buffer[i].config.headers.Authorization = 'bearer' + ' ' + token;
						retryHttpRequest(buffer[i].config, buffer[i].deferred);
					}
				},


			};
		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('TagService', ['$http', '$q', '$cookieStore', '$stateParams', '$location', 'FlashService', 'site', function($http, $q, $cookieStore, $stateParams, $location, FlashService, site) {
			var tagService = {};

			tagService.getDataByTag = function (position, tagTitle) {
				var promise = $http({
					url: site + '/api/posts/' + '?offset=' + position.offset + '&count=' + position.count + '&tag_title=' + tagTitle,
					method: 'GET'
				});

				return promise;
			};

			tagService.getPopularTags = function () {
				var promise = $http({
					url: site + '/api/tags?best=10',
					method: 'GET'
				});

				return promise;
			};

			return tagService;

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('ToastrService', [function () {
			toastr.options = {

			};

			return {
				success: function (message) {
					toastr.success(message);
				},
				warning: function (message) {
					toastr.warning(message);
				},
				error: function (message) {
					toastr.error(message);
				},
				info: function (message) {
					toastr.info(message);
				}
			};
		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('GroupService', ['$http', '$q', '$stateParams', '$timeout', 'Upload', 'site', function($http, $q, $stateParams, $timeout, Upload, site) {

			var groupService = {};

			groupService.getGroups = function(position) {
				var promise = $http({
					url: site + '/api/groups?offset=' + position.offset + '&count=' + position.count,
					method: 'GET'
				});

				return promise;
			};

			groupService.createGroup = function(groupData, file) {
				var fileData = '';
				if (!file) {
					fileData = {};
				} else {
					fileData = file;
				}

				var promise = Upload.upload({
					url: site + '/api/groups',
					method: 'POST',
					headers: {
						'Content-Type': 'multipart/form-data'
					},
					fields: groupData,
					file: fileData
				});

				return promise;
			};

			groupService.follow = function(groupId, is_follow) {
				var promise = $http({
					url: site + '/api/groups/' + groupId + '?follow=' + !is_follow,
					method: 'PUT'
				});

				return promise;
			};

			groupService.followedGroups = function(userId) {
				var promise = $http({
					url: site + '/api/groups?followed_by=' + userId,
					method: 'GET'
				});

				return promise;
			};

			return groupService;

		}]);
})();
(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('AuthInterceptor', ['$q', '$rootScope', 'RequestStorage', function ($q, $rootScope, RequestStorage) {
			return {
				responseError: function(response) {
					if (response.status == '401') {
						var deferred = $q.defer();
						RequestStorage.push(response.config, deferred);
						$rootScope.$broadcast('event:auth-loginRequired');
						return deferred.promise;
					}
					else if (response.status == '403') {
						$rootScope.$broadcast('event:auth-forbidden', response);
					}

					return $q.reject(response);
				}
			};
		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('PostService', ['$http', '$q', '$stateParams', '$timeout', '$rootScope', 'Upload', 'ModalService', 'site', function($http, $q, $stateParams, $timeout, $rootScope, Upload, ModalService, site) {

			var postService = {};

			postService.getPosts = function(groupId, position) {
				var promise = $http({
					url: site + '/api/groups/' + groupId + '/posts' + '?offset=' + position.offset + '&count=' + position.count,
					method: 'GET'
				});

				return promise;
			};

			postService.checkGroup = function() {
				var promise = $http({
					url: site + '/api/groups/' + $stateParams.id,
					method: 'GET'
				});

				return promise;
			};

			postService.createPost = function (postData, groupId) {
				var promise = $http({
					url: site + '/api/groups/' + groupId + '/posts',
					method: 'POST',
					data: postData
				});

				return promise;
			};

			postService.like = function (postId, is_liked) {
				var promise = $http({
					url: site + '/api/posts/' + postId + '?like=' + !is_liked,
					method: 'PUT'
				});

				return promise;
			};

			return postService;

		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('AuthService', ['$window', '$http', '$q', '$stateParams', '$rootScope', 'site', function($window, $http, $q, $stateParams, $rootScope, site) {

			var authService = {};

			authService.login = function(user) {
				var promise = $http({
					url: site + '/token',
					data: 'username=' + user.email + '&' + 'password=' + user.password + '&' + 'grant_type=password',
					method: 'POST',
					cache: false,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				});

				return promise;
			};

			authService.logout = function() {
				var deferred = $q.defer();
				deferred.resolve();
				return deferred.promise;
			};

			return authService;


		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('UserApiService', ['$cookieStore', '$rootScope', function($cookieStore, $rootScope) {

			var userApiService = {};

			userApiService.setCredentials = function(user, token) {
				$rootScope.globals = {
					currentUser: {
						username: user.email,
						token: token
					}
				};

				$cookieStore.put('globals', $rootScope.globals);
				$rootScope.$broadcast('event:auth-loginConfirmed');
			};

			userApiService.clearCredentials = function() {
				$rootScope.globals = {};
				$cookieStore.remove('globals');
			};

			userApiService.isAuth = function() {
				return !!$rootScope.globals.currentUser.username;
			};

			return userApiService;


		}]);
})();
(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('UserService', ['$http', '$q', '$stateParams', '$location', 'site', function($http, $q, $stateParams, $location, site) {

			var userService = {};

			userService.create = function(user) {
				var promise = $http({
						url: site + '/api/account',
						data: user,
						method: 'POST'
					});

					return promise;
			};

			userService.edit = function(userId) {

			};

			userService.remove = function(userId) {

			};

			userService.groups = function(userId) {
				var promise = $http({
					url: site + '/api/users/' + userId + '/groups',
					method: 'GET'
				});
			};

			return userService;

		}]);
})();