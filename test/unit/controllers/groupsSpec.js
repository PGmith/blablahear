describe('Groups: controller', function() {

	var response = {};
	response.data = [{title: 'Test1'}, {title: 'Test2'}, {title: 'Test3'}, {title: 'Test4'}];
	
	var scope = '';
	var q = '';
	var deferred = '';
	var groupsCtrl = '';
	var _mockGroupService_ = '';

	beforeEach(module('blablaApp'));

	beforeEach(module(function ($provide) {
		$provide.factory('GroupService', ['$q', function ($q) {
			return {
				getGroups: function () {
					return $q.when(response);
				},
				createGroup: function() {
					return $q.when({title: 'Test5'});
				}
			};
		}]);
	}));

	beforeEach(inject(function($rootScope, $controller, GroupService) {
		rootScope = $rootScope;
		scope = $rootScope.$new();
		_mockGroupService_ = GroupService;

		groupsCtrl = $controller('GroupsCtrl', {
			$scope: scope,
			GroupService: _mockGroupService_
		});

	}));


	it('should have group items', function() {
		scope.$digest();
		var data = groupsCtrl.data.groups;

		expect(data).not.toBe([]);
	});

	it('should create group', function () {
		groupsCtrl.createGroup();
		scope.$digest();
		var data = groupsCtrl.data.groups;

		expect(data.length).toEqual(5);
	});

	it('should get offset groups', function () {
		spyOn(_mockGroupService_, 'getGroups').and.callFake(function () {
			var response = {};
			response.data = [{title: 'Test1'}, {title: 'Test2'}, {title: 'Test3'}, {title: 'Test4'}];
			return $q.when(response);
		});
		groupsCtrl.nextScroll();
		scope.$digest();

		var data = groupsCtrl.data.groups;

		expect(data.length).toEqual(9);
	});
});