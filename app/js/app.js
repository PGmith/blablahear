(function() {
	'use strict';

	angular.module('blablaApp', ['ui.router', 'ngCookies', 'ngFileUpload'])
		.constant('site', 'http://blablahear.azurewebsites.net')
		.run(['$rootScope', '$location', '$cookieStore', '$http', '$state', '$window', 'RequestStorage', 'ModalService', 'ToastrService', function($rootScope, $location, $cookieStore, $http, $state, $window, RequestStorage, ModalService, ToastrService) {
			$rootScope.globals = $cookieStore.get('globals') || {};

			if ($rootScope.globals.currentUser) {
				$http.defaults.headers.common.Authorization = 'bearer' + ' ' + $rootScope.globals.currentUser.token;
			}

			$rootScope.$on('event:auth-loginRequired', function() {
				ModalService.open('modal-hybrid');
			});

			$rootScope.$on('event:auth-loginConfirmed', function() {
				if (RequestStorage.bufferSize() > 0) {
					RequestStorage.retryAllWithToken($rootScope.globals.currentUser.token);
				}
				$http.defaults.headers.common.Authorization = 'bearer' + ' ' + $rootScope.globals.currentUser.token;
				ToastrService.success('Добро пожаловать');
			});

			$rootScope.$on('event:auth-loginCanceled', function() {
				if (RequestStorage.bufferSize() > 0) {
					RequestStorage.rejectAll('Incorrect login or password');
				}
			});

			$rootScope.$on('event:auth-forbidden', function() {
				ToastrService.warning('Доступ запрещен');
			});

			$rootScope.$on('event:user-UserCreated', function() {
				ToastrService.success('Регистрация прошла успешно');
			});

		}])
		.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'site', function($stateProvider, $urlRouterProvider, $httpProvider, site) {
			$httpProvider.interceptors.push('AuthInterceptor');
			$stateProvider
				.state('users', {
					url: '/users',
					abstract: true,
					templateUrl: 'app/templates/users/users.html'
				})
				.state('users.groups', {
					url: '/:id/groups',
					templateUrl: 'app/templates/users/user_groups.html'
				})
				.state('groups', {
					url: '/groups?offset&count',
					templateUrl: 'app/templates/groups_list.html'
				})
				.state('posts', {
					url: '/groups/:id?offset&count',
					templateUrl: 'app/templates/posts_list.html'
				})
				.state('tags', {
					url: '/posts?q',
					templateUrl: 'app/templates/tags_list.html'
				});

			$urlRouterProvider.otherwise('/groups');
			$urlRouterProvider.when('/', '/groups');
			$urlRouterProvider.when('', '/groups');
		}]);

})();