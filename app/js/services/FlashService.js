(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('FlashService', ['$rootScope', function($rootScope) {

			(function() {
				$rootScope.$on('$locationChangeStart', function() {
					clearFlashMessage();
				});

				function clearFlashMessage() {
					var flash = $rootScope.flash;
					if (flash) {
						if (!flash.keepAfterLocationChange) {
							delete $rootScope.flash;
						} else {
							flash.keepAfterLocationChange = false;
						}
					}

				}
			})();

			return {
				success: function(keepAfterLocationChange) {
					$rootScope.flash = {
						message: 'Succes',
						type: 'success',
						keepAfterLocationChange: keepAfterLocationChange
					};
				},
				error: function(keepAfterLocationChange) {
					$rootScope.flash = {
						message: 'Error was occured',
						type: 'error',
						keepAfterLocationChange: keepAfterLocationChange
					};
				}
			};

		}]);
})();