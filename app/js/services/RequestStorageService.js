(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('RequestStorage', ['$injector', function ($injector) {
			var buffer = [];

			function retryHttpRequest(config, deferred) {
				function successCallback(response) {
					deferred.resolve(response);
				}

				function errorCallback(response) {
					deferred.reject(response);
				}
				var $http = $http || $injector.get('$http');
				$http(config).then(successCallback, errorCallback);
			}

			return {
				push: function(config, deferred) {
					buffer.push({
						config: config,
						deferred: deferred
					});
				},
				bufferSize: function () {
					return buffer.length;
				},
				rejectAll: function(reason) {
					if (reason) {
						for (var i = 0; i < buffer.length; i++) {
							buffer[i].deferred.reject(reason);
						}
					}
					buffer = [];
				},
				retryAllWithToken: function(token) {
					for (var i = 0; i < buffer.length; i++) {
						buffer[i].config.headers.Authorization = 'bearer' + ' ' + token;
						retryHttpRequest(buffer[i].config, buffer[i].deferred);
					}
				},


			};
		}]);
})();