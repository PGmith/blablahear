(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('GroupService', ['$http', '$q', '$stateParams', '$timeout', 'Upload', 'site', function($http, $q, $stateParams, $timeout, Upload, site) {

			var groupService = {};

			groupService.getGroups = function(position) {
				var promise = $http({
					url: site + '/api/groups?offset=' + position.offset + '&count=' + position.count,
					method: 'GET'
				});

				return promise;
			};

			groupService.createGroup = function(groupData, file) {
				var fileData = '';
				if (!file) {
					fileData = {};
				} else {
					fileData = file;
				}

				var promise = Upload.upload({
					url: site + '/api/groups',
					method: 'POST',
					headers: {
						'Content-Type': 'multipart/form-data'
					},
					fields: groupData,
					file: fileData
				});

				return promise;
			};

			groupService.follow = function(groupId, is_follow) {
				var promise = $http({
					url: site + '/api/groups/' + groupId + '?follow=' + !is_follow,
					method: 'PUT'
				});

				return promise;
			};

			groupService.followedGroups = function(userId) {
				var promise = $http({
					url: site + '/api/groups?followed_by=' + userId,
					method: 'GET'
				});

				return promise;
			};

			return groupService;

		}]);
})();