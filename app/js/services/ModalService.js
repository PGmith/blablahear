(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('ModalService', [function () {
		
			return {
				close: function () {
					$('.modal').modal('hide');
				},
				open: function(name) {
					$('.' + name).modal('show');
				}
			};
		}]);
})();