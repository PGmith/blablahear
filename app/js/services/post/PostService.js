(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('PostService', ['$http', '$q', '$stateParams', '$timeout', '$rootScope', 'Upload', 'ModalService', 'site', function($http, $q, $stateParams, $timeout, $rootScope, Upload, ModalService, site) {

			var postService = {};

			postService.getPosts = function(groupId, position) {
				var promise = $http({
					url: site + '/api/groups/' + groupId + '/posts' + '?offset=' + position.offset + '&count=' + position.count,
					method: 'GET'
				});

				return promise;
			};

			postService.checkGroup = function() {
				var promise = $http({
					url: site + '/api/groups/' + $stateParams.id,
					method: 'GET'
				});

				return promise;
			};

			postService.createPost = function (postData, groupId) {
				var promise = $http({
					url: site + '/api/groups/' + groupId + '/posts',
					method: 'POST',
					data: postData
				});

				return promise;
			};

			postService.like = function (postId, is_liked) {
				var promise = $http({
					url: site + '/api/posts/' + postId + '?like=' + !is_liked,
					method: 'PUT'
				});

				return promise;
			};

			return postService;

		}]);
})();