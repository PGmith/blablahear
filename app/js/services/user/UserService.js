(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('UserService', ['$http', '$q', '$stateParams', '$location', 'site', function($http, $q, $stateParams, $location, site) {

			var userService = {};

			userService.create = function(user) {
				var promise = $http({
						url: site + '/api/account',
						data: user,
						method: 'POST'
					});

					return promise;
			};

			userService.edit = function(userId) {

			};

			userService.remove = function(userId) {

			};

			userService.groups = function(userId) {
				var promise = $http({
					url: site + '/api/users/' + userId + '/groups',
					method: 'GET'
				});
			};

			return userService;

		}]);
})();