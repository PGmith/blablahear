(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('UserApiService', ['$cookieStore', '$rootScope', function($cookieStore, $rootScope) {

			var userApiService = {};

			userApiService.setCredentials = function(user, token) {
				$rootScope.globals = {
					currentUser: {
						username: user.email,
						token: token
					}
				};

				$cookieStore.put('globals', $rootScope.globals);
				$rootScope.$broadcast('event:auth-loginConfirmed');
			};

			userApiService.clearCredentials = function() {
				$rootScope.globals = {};
				$cookieStore.remove('globals');
			};

			userApiService.isAuth = function() {
				return !!$rootScope.globals.currentUser.username;
			};

			return userApiService;


		}]);
})();