(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('AuthService', ['$window', '$http', '$q', '$stateParams', '$rootScope', 'site', function($window, $http, $q, $stateParams, $rootScope, site) {

			var authService = {};

			authService.login = function(user) {
				var promise = $http({
					url: site + '/token',
					data: 'username=' + user.email + '&' + 'password=' + user.password + '&' + 'grant_type=password',
					method: 'POST',
					cache: false,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				});

				return promise;
			};

			authService.logout = function() {
				var deferred = $q.defer();
				deferred.resolve();
				return deferred.promise;
			};

			return authService;


		}]);
})();