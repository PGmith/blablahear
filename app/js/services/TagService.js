(function() {
	'use strict';

	angular.module('blablaApp')
		.factory('TagService', ['$http', '$q', '$cookieStore', '$stateParams', '$location', 'FlashService', 'site', function($http, $q, $cookieStore, $stateParams, $location, FlashService, site) {
			var tagService = {};

			tagService.getDataByTag = function (position, tagTitle) {
				var promise = $http({
					url: site + '/api/posts/' + '?offset=' + position.offset + '&count=' + position.count + '&tag_title=' + tagTitle,
					method: 'GET'
				});

				return promise;
			};

			tagService.getPopularTags = function () {
				var promise = $http({
					url: site + '/api/tags?best=10',
					method: 'GET'
				});

				return promise;
			};

			return tagService;

		}]);
})();