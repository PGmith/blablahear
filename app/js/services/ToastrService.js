(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('ToastrService', [function () {
			toastr.options = {

			};

			return {
				success: function (message) {
					toastr.success(message);
				},
				warning: function (message) {
					toastr.warning(message);
				},
				error: function (message) {
					toastr.error(message);
				},
				info: function (message) {
					toastr.info(message);
				}
			};
		}]);
})();