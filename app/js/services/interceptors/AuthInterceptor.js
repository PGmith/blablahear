(function () {
	'use strict';

	angular.module('blablaApp')
		.factory('AuthInterceptor', ['$q', '$rootScope', 'RequestStorage', function ($q, $rootScope, RequestStorage) {
			return {
				responseError: function(response) {
					if (response.status == '401') {
						var deferred = $q.defer();
						RequestStorage.push(response.config, deferred);
						$rootScope.$broadcast('event:auth-loginRequired');
						return deferred.promise;
					}
					else if (response.status == '403') {
						$rootScope.$broadcast('event:auth-forbidden', response);
					}

					return $q.reject(response);
				}
			};
		}]);
})();