(function() {
	'use strict';

	angular.module('blablaApp')
		.directive('resetForm', ['$timeout', 'ModalService', function($timeout, ModalService) {
			return {
				restrict: 'A',
				require: '^form',
				scope: {
					status: '=',
					formData: '='
				},
				link: function(scope, $elem, $attrs, formCtrl) {
					scope.$watch(function() {
						return scope.status;
					}, function() {
						if (scope.status) {
							ModalService.close();
							formCtrl.$setPristine();
							scope.formData = {};
						}
					});

				}
			};
		}]);

	angular.module('blablaApp')
		.directive('passwordMatch', ['$timeout', function($timeout) {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function(scope, $elem, $attrs, ctrl) {
					$elem.add('.pw1').on('keyup', function() {
						scope.$apply(function() {
							var v = $elem.val() === $('.pw1').val();
							ctrl.$setValidity('pwmatch', v);
						});
					});
				}
			};
		}]);

	angular.module('blablaApp')
		.directive('endlessScroll', ['$timeout', function($timeout) {
			return {
				restrict: 'A',
				scope: {
					nextScroll: '&'
				},
				link: function(scope, $elem, $attrs) {
					$(window).on('scroll', function() {
						if ($(window).scrollTop() === $(document).height() - $(window).height()) {
							scope.nextScroll();
							// if (scope.group.config.allDataLoaded)
							// 	$(window).off('scroll');
						}
					});
				}
			};
		}]);

	angular.module('blablaApp')
		.directive('ngFocus', [function() {
			var FOCUS_CLASS = "ng-focused";
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function(scope, $elem, attrs, ctrl) {
					ctrl.$focused = false;
					$elem.on('focus', function(event) {
						$elem.addClass(FOCUS_CLASS);
						scope.$apply(function() {
							ctrl.$focused = true;
						});
					});
					$elem.on('blur', function(event) {
						$elem.removeClass(FOCUS_CLASS);
						scope.$apply(function() {
							ctrl.$focused = false;
						});
					});
				}
			};
		}]);

	angular.module('blablaApp')
		.directive("masonry", ['$timeout', function($timeout) {
			return {
				scope: {
					itemSelector: '@'
				},
				link: function(scope, elem, attrs) {

					scope.$watch(function() {
							return elem[0].children.length;
						},
						function(newVal) {
							$timeout(function() {
								elem.masonry('reloadItems');
								elem.masonry();
							}, 200);
						});

					elem.masonry({
						itemSelector: scope.itemSelector,
						transitionDuration: 0
					});
					scope.masonry = elem.data('masonry');
				}
			};
		}]);

	angular.module('blablaApp')
		.directive('tabs', [function() {
			return {
				restrict: 'E',
				replace: true,
				transclude: true,
				scope: {
					paneTitles: '=',
					paneAnchors: '=',
					active: '=',
					mainClass: '='
				},
				controller: ['$scope', function($scope) {
					this.paneTitles = [];
					this.paneAnchors = [];
					this.active = $scope.active;
					this.paneTitles = $scope.paneTitles;
					this.paneAnchors = $scope.paneAnchors;
					$scope.select = function(anchor) {
						$scope.active = anchor;
						this.active = $scope.active;
					};
				}],
				link: function(scope, $elem, attrs, ctrl) {

				},
				template: 
				"<div>" +
					"<ul class='nav nav-tabs {{ class }}'>" +
						"<li ng-repeat='pane in paneTitles track by $index' ng-click='select(paneAnchors[$index])'>" +
							"<a href='#{{ paneAnchors[$index] }}' data-toggle='tab'>{{ pane }}</a>" +
						"</li>" +
					"</ul>" +
					"<div class='tab-content' ng-transclude></div>" +
				"</div>"
			};
		}]);

	angular.module('blablaApp')
		.directive('pane', [function() {
			return {
				restrict: 'E',
				replace: true,
				transclude: true,
				scope: {
					anchor: '@'
				},
				controller: ['$scope', function($scope) {
					this.anchor = $scope.anchor;
				}],
				template: "<div class='tab-pane' id='{{ anchor }}' ng-transclude></div>"
			};
		}]);


})();