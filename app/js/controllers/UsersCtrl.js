(function() {
	'use strict';

	angular.module('blablaApp')
		.controller('UsersCtrl', ['$scope', '$state', '$rootScope', '$location', 'UserService', 'UserApiService', function($scope, $state, $rootScope, $location, UserService, UserApiService) {
			var vm = this;
			vm.data = {};
			vm.data.user = '';
			vm.data.status = false;


			vm.register = function() {
				vm.data.status = false;
				UserService.create(vm.data.user)
					.then(function(response) {
						vm.data.status = true;
						$rootScope.$broadcast('event: auth-UserCreated');
					});
			};


		}]);
})();