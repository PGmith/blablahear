(function() {
	'use strict';

	angular.module('blablaApp')
		.controller('AuthCtrl', ['$scope', '$state', '$rootScope', '$location', '$window', 'AuthService', 'UserApiService', function($scope, $state, $rootScope, $location, $window, AuthService, UserApiService) {
			var vm = this;
			vm.data = {};
			vm.data.user = '';
			vm.data.status = false;

			vm.login = function() {
				vm.data.status = false;
				AuthService.login(vm.data.user)
					.then(function(response) {
						vm.data.status = true;
						UserApiService.setCredentials(vm.data.user, response.data.access_token);
					}, function(response) {
						vm.data.status = true;
						$rootScope.$broadcast('event:auth-loginCanceled');
					});
			};

			$rootScope.logout = function () {
				AuthService.logout().then(function() {
					$window.location.reload();
					UserApiService.clearCredentials();
				});
			};

		}]);
})();