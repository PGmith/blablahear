(function() {
	'use strict';

	angular.module('blablaApp')
		.controller('GroupsCtrl', ['$scope', '$timeout', 'GroupService', 'ToastrService', function($scope, $timeout, GroupService, ToastrService) {
			var vm = this;
			vm.data = {};
			vm.position = {};
			vm.position.offset = 0;
			vm.position.count = 10;
			vm.data.groups = [];
			vm.data.groupData = {};
			vm.data.loaded = false;
			vm.data.status = false;


			GroupService.getGroups(vm.position)
				.then(function(response) {
					vm.data.groups = response.data;
					vm.data.loaded = true;
				});

			vm.createGroup = function (file) {
				vm.data.status = false;
				GroupService.createGroup(vm.data.groupData, file)
					.then(function(response) {
						vm.data.groups.push(response.data);
						vm.data.status = true;
						ToastrService.success('Группа ' + response.data.short_name + ' была успешно создана');
					});
			};

			vm.follow = function (index, id, is_follow) {
				GroupService.follow(id, is_follow)
					.then(function (response) {
						if (is_follow)
							vm.data.groups[index].followers_count--;
						else 
							vm.data.groups[index].followers_count++;

						vm.data.groups[index].is_follow = !vm.data.groups[index].is_follow;
					});	
			};

			vm.nextScroll = function() {
				vm.data.loaded = false;
				vm.position.offset += 10;
				$timeout(function() {
					GroupService.getGroups(vm.position)
						.then(function(response) {
							vm.data.groups = vm.data.groups.concat(response.data);
							vm.data.loaded = true;
						});
				}, 300);
			};

		}]);
})();