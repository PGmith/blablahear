(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('PostsCtrl', ['$scope', '$timeout', '$stateParams', 'PostService', 'ToastrService', function ($scope, $timeout, $stateParams, PostService, ToastrService) {
			var vm = this;
			vm.data = {};
			vm.position = {};
			vm.position.offset = 0;
			vm.position.count = 10;
			vm.data.posts = [];
			vm.data.postData = {};
			vm.data.loaded = false;
			vm.data.status = false;
			vm.data.groupId = '';
			vm.data.groupName = '';

			PostService.checkGroup()
				.then(function (response) {
					vm.data.groupId = response.data.id;
					vm.data.groupName = response.data.full_name;
					PostService.getPosts(vm.data.groupId, vm.position)
						.then(function (response) {
							vm.data.posts = response.data;
							vm.data.loaded = true;
						});
				});

			vm.createPost = function () {
				vm.data.status = false;
				PostService.createPost(vm.data.postData, vm.data.groupId)
					.then(function (response) {
						vm.data.posts.push(response.data);
						vm.data.status = true;
						ToastrService.success('Сообщение добавлено');
					});
			};

			vm.like = function (index, id, is_liked) {
				PostService.like(id, is_liked)
					.then(function (response) {
						if (is_liked)
							vm.data.posts[index].likes--;
						else 
							vm.data.posts[index].likes++;

						vm.data.posts[index].is_liked = !vm.data.posts[index].is_liked;
					});	
			};

			vm.nextScroll = function() {
				vm.data.loaded = false;
				vm.position.offset += 10;
				$timeout(function() {
					PostService.getPosts(vm.data.groupId, vm.position)
						.then(function(response) {
							vm.data.posts = vm.data.posts.concat(response.data);
							vm.data.loaded = true;
						});
				}, 300);
			};

		}]);
})();