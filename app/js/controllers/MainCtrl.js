(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('MainCtrl', ['$scope', '$rootScope', 'TagService', function ($scope, $rootScope, TagService) {
			$rootScope.showPanel = false;
			$rootScope.section = 'groups';

			$rootScope.$on('$stateChangeSuccess', function (event, toState) {
				$rootScope.section = toState.name;
			});

		}]);
})();