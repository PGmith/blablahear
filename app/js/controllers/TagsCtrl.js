(function () {
	'use strict';

	angular.module('blablaApp')
		.controller('TagsCtrl', ['$scope', '$timeout', '$stateParams', 'TagService', function ($scope, $timeout, $stateParams, TagService) {
			var vm = this;
			vm.data = {};
			vm.data.dataByTag = [];
			vm.data.popularTags = [];
			vm.data.loaded = false;
			vm.data.tagTitle = $stateParams.q;
			vm.position = {};
			vm.position.offset = 0;
			vm.position.count = 10;

			if ($stateParams.q) {
				TagService.getDataByTag(vm.position, vm.data.tagTitle)
					.then(function (response) {
						vm.data.dataByTag = response.data;
						vm.data.loaded = true;
					});
			} else {
				TagService.getPopularTags()
					.then(function(response) {
						vm.data.popularTags = response.data;
					});
			}
			

		}]);
})();