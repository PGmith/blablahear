(function() {
    'use strict';

    angular.module('blablaApp')
        .filter('dateFomat', ['$filter', function($filter) {
            return function (content) {
            	moment.locale("ru");
            	var date = moment(content).fromNow();
                return date;
            };
        }]);
})();