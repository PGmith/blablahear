(function() {
    'use strict';

    angular.module('blablaApp')
        .filter('hashtag', ['$filter', '$sce', function($filter, $sce) {
            return function (content) {
                var newContent = content.replace(/(#)(\w*[a-zA-Z_]+\w*)/gim, "<a ui-sref='tags({q: $2})' href='#/posts?q=$2'>$&</a>");
                return $sce.trustAsHtml(newContent);
            };
        }]);
})();