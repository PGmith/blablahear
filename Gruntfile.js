module.exports = function(grunt) {

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		jshint: {
			options: {
				reporter: require('jshint-stylish')
			},
			prduction: ['Gruntfile.js', 'app/js/**/*.js'],
			development: ['Gruntfile.js', 'app/js/**/*.js']
		},

		uglify: {
			options: {
				banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
			},
			prduction: {
				files: {
					'dist/js/application.lib.min.js': 'dev/js/application.lib.js',
					'dist/js/application.min.js': 'dev/js/application.js'
				}
			},
			development: {
				files: {
					'dist/js/application.min.js': 'dev/js/application.js'
				}
			}
		},

		less: {
			prduction: {
				files: {
					'app/css/application.css': 'app/css/*.less'
				}
			},
			development: {
				files: {
					'app/css/application.css': 'app/css/*.less'
				}
			}
		},

		cssmin: {
			options: {
				banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
			},
			prduction: {
				files: {
					'dist/css/application.min.css': ['vendor/css/*.css', 'app/css/application.css']
				}
			},
			development: {
				files: {
					'dist/css/application.min.css': ['vendor/css/*.css', 'app/css/application.css']
				}
			}
		},

		concat: {
			prduction: {
				files: {
					'dev/js/application.lib.js': [
						'vendor/js/jquery/jquery.min.js',
						'vendor/js/underscore/*.js',
						'vendor/js/angular/angular.min.js',
						'vendor/js/angular/*.js',
						'vendor/js/bootstrap/*.js',
						'vendor/js/toastr/*.js',
						'vendor/js/masonry/*.js',
						'vendor/js/moment/*.js'
					],
					'dev/js/application.js': ['app/js/**/*.js']
				}
			},
			development: {
				files: {
					'dev/js/application.js': ['app/js/**/*.js']
				}
			}
		},

		ngAnnotate: {
			prduction: {
				files: {
					'dist/js/application.min.js': ['vendor/js/angular/*.js', 'app/js/**/*.js']
				}
			},
			development: {
				files: {
					'dist/js/application.min.js': ['app/js/**/*.js']
				}
			}
		},
		watch: {
			// options: {
			// 	livereload: {
			// 		port: 9000,
			// 		hostname: 'localhost/app/index.html'
			// 	}
			// },
			css: {
				files: ['app/css/*.less'],
				tasks: ['less:development', 'cssmin:development'],
				options: {
					spawn: false
				}
			},
			js: {
				files: ['app/js/**/*.js'],
				tasks: ['jshint:development', 'ngAnnotate:development', 'concat:development', 'uglify:development'],
				options: {
					spawn: false
				}
			},
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['jshint:development', 'ngAnnotate:development', 'concat:development', 'uglify:development'],
				options: {
					spawn: false
				}
			}
		}

	});

	// ======= TASKS =========== ///

	grunt.registerTask('default', ['jshint', 'ngAnnotate', 'concat', 'uglify', 'cssmin', 'less']);
	grunt.registerTask('prduction', ['jshint:prduction', 'ngAnnotate', 'concat', 'uglify:prduction', 'cssmin:prduction', 'less:prduction']);
	grunt.registerTask('development', ['jshint:development', 'ngAnnotate:development', 'concat:development', 'uglify:development', 'cssmin:development', 'less:development']);

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-serve');
	grunt.loadNpmTasks('grunt-ng-annotate');

};